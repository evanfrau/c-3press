# Cypress project

This is a project for the Automation Battle Guild.

## What is Cypress

[Cypress](https://cypress.io) is a testing tool written in JavaScript.
Used for Frontend E2E testing. API testing is also possible.

- Free
- Open Source
- No selenium
- Visual interface (tests with commands on the left and App on the right)
- DOM Snapshot history
- Use the JavaScript library [Mocha](https://mochajs.org/) to write tests
- Use the assertation library [Chai](https://www.chaijs.com/)
- Both libraries embrace BDD and TDD assertion styles

[Link to Cypress documentation](https://docs.cypress.io/guides/core-concepts/introduction-to-cypress#Cypress-is-Like-jQuery)

## Lessons summary
- [Lesson 1](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-1): How to install Cypress
- [Lesson 2](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-2): How to run Cypress
- [Lesson 3](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-3): How to run Cypress using the command terminal
- [Lesson 4](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-4): Organize and Write a first test case
- [Lesson 5](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-5): Refactor the test
- [Lesson 6](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-6): How to use Cypress build-in reporting
- [Lesson 7](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-7): How to add an HTML report
- [Lesson 8](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-8): More advanced Cypress methods

------

## Features and Tests scenarios

Your mission is to install Cypress and test [Starflix](https://starflix-zeta.vercel.app/), website dedicated to Star Wars using the [SWAPI api](https://swapi.dev)

Features of Starflix:

- Login to access the platform (browser session)
- Change Name and Password
- See the list of the Star Wars films
- See the details of each films (summary, casting)
- See the details of each characters
- Search for a film or a character
- Add a film to the watchlist
- Remove a film from the watchlist

_Link:_ https://starflix-zeta.vercel.app/  

### Tests scenarios

__Important notes__

Use `padawan` as name & `force` as password for the initial login. 
If you change the credentials and forget, clear the cache to reset the credentials.

4 modules: Login, consultation, search, watchlist

1. __Simple tests (6)__
- _User can login and change his name & password (also assert empty and error state)_ (+2)
- _User can see the details of a film (also test the url)_ (+2)
- _User can add a film to the watchlist_ (+2)

2. __Medium tests using customs commands and hooks (6)__ 
- _User can login and see the details of a film_ (+3)
- _User can add a film to the watchlist and remove it from the watchlist_ (use hooks to clear the state) (+3)

3. __Advanced tests using mocks (5)__ 
- _User can search for a film or a character_ (use fixtures to mock the response, you can use [SWAPI api](https://swapi.dev)) (+5) 

__Rules (3)__

1. Create folders per module
2. Create files per suite (eg: Login > login.spec.js & settings.spec.js)
3. Use data-cy attribute when available (using the Selector Playground)
4. Duplicate one of the tests and make it fail (to see a failure with the screenshots in the report)
5. Deactivate the video recording
6. Zip the project without the node modules and examples folders
7. Include the test results from terminal or from the html report in the file


__Contact__

If something is unclear, you can always contact me: _emeric.van-frausum@sogeti.com_


-------

## IDE used
[Visual Studio Code](https://code.visualstudio.com/)

## Install the project from cloning this project

Clone the project...

```bash
git clone 
```

Install the dependencies...

```bash
npm install 
```

...then run Cypress

```bash
npm run cy:open 
```
## Or you can follow the lessons

Start the first lesson: [Install Cypress from scratch](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-1)
